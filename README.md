## Hardshield Oasis

_A map for Supreme Commander: Forged Alliance (Forever)_

![](/images/impression01.png)

_AAfter years of pollution, bot after bot, war after war, numerous planets turned slowly turned into desert lands due to an human-reinforced natural process called desertification. Entire planets turned into giant desert, losing their might and worth. Environment activists did not agree and initially started working on environment-preserving technologies years ago. No progress was made up to the point where the knowledge of the factions was combined and a terraformer was being built. Here, on planet B42, the first protoype of this terraformer is slowly turning a desert into a flourishing forest by spreading countless nanobots whom are designed to actively construct vegetation such as trees. After years of being hidden, this prototype has been found and is now another reason to continue the war: Whom will own the prototype and be able to repair their planets with this astonishing new technology?._

## Story surrounding the map

![](/images/impression02.png)

The story of the map circles around the desertification that is happening across the globe. The process is accelerated due to climate change. Entire regions that were first flourishing or arid lands turned into stone cold deserts with barely any vegetation to be seen. 

## Statistics of the map

![](/images/impression03.png)

The map is designed to be either be played as a 1 vs 1 or 2 vs 2. Both teams start on an identical plateau, from which they have to expand to the sides of the map. The centre of the map contains a so-called terraformer. An additional 4 - 10 mass for each player can be found outside the base, depending on where they meet with the enemy.

There is mass reclaim on this map. This includes:
 - A few rocks that are used as scenery.
 - A few units (t1 tanks / t1 bombers). This is specifically true for the player that starts with less mass extractors.

The map is sufficient in dynamics (number of available paths to other players) to allow for diverse and interesting gameplay.

![](/images/impression04.png)

The map has various pieces of code to add additional feeling to the map. This includes:
 - All initial wreckages can be on fire.
 - Two trucks at every spawn location drive off shortly after the commanders spawn.
 - The 'terraformer' in the middle, along with various emitters.

The map has over 2300 decals in order to make it pretty. Yes, that is a lot of decals.

## Notes about the imagery

 - The units with yellow ('Golden Rod') icons represent the terraformer at the centre.
 - The units with white icons represent the neutral civilians.
 - The units with nearly black icons represent the reclaim that is available on the map.

## More imagery

### The Terraformer
![](/images/impression05.png)

### Fire on wreckages
![](/images/impression06.png)

### Overview
![](/images/overview.png)

## License

The stratum layers (/env/layers) are from www.textures.com. I am obligated to add this text:
_One or more textures bundled with this project have been created with images from Textures.com. These images may not be redistributed by default. Please visit www.textures.com for more information._

All other assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
