version = 3
ScenarioInfo = {
    name = 'Adaptive Hardshield Oasis',
    description = 'Made by (Jip) Willem Wijnia. Submission for the mapping tournament 3 (AU Summer of 2020). You can find the latest version at: https://gitlab.com/w.b.wijnia/desified ',
    type = 'skirmish',
    starts = true,
    preview = '',
    map_version = 1,
    size = {256, 256},
    map = '/maps/adaptive_hardshield_oasis/adaptive_hardshield_oasis.scmap',
    save = '/maps/adaptive_hardshield_oasis/adaptive_hardshield_oasis_save.lua',
    script = '/maps/adaptive_hardshield_oasis/adaptive_hardshield_oasis_script.lua',
    norushradius = 0.000000,
    norushoffsetX_ARMY_1 = 0.000000,
    norushoffsetY_ARMY_1 = 0.000000,
    norushoffsetX_ARMY_2 = 0.000000,
    norushoffsetY_ARMY_2 = 0.000000,
    norushoffsetX_ARMY_3 = 0.000000,
    norushoffsetY_ARMY_3 = 0.000000,
    norushoffsetX_ARMY_4 = 0.000000,
    norushoffsetY_ARMY_4 = 0.000000,
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'ARMY_1','ARMY_2','ARMY_3','ARMY_4',} },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 Wreckages Neutrals ARMY_9 NEUTRAL_CIVILIAN' ),
            },
        },
    }}
