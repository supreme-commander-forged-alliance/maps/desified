
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/desified
-- 
-- Please do not remove this message
--------------------------------------------------------------------------------


do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- find all players

	local armies = ListArmies()
	local players = { }
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army)
		end
	end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- set all the diplomacy

    -- between the structure in the middle and the typical neutrals
    SetAlliance("Neutrals", "NEUTRAL_CIVILIAN", "ally")

    -- between players and the structure in the middle, this allows the shield to catch bullets
    for k, army in players do
        SetAlliance(army, "Neutrals", "Enemy")
    end

end
