
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/desified
-- 
-- Please do not remove this message
--------------------------------------------------------------------------------

local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- find all players

	local armies = ListArmies()
	local players = { }
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army)
		end
    end
    
    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- spawn the obsidians

    -- spawn the obsidians
    local obsidians = ScenarioUtils.CreateArmyGroup("Capturable", "Obsidians", false)

    -- damage them and set a small veterancy
    for k, unit in obsidians do 

        -- set health
        unit:SetHealth(nil, 310)

        -- give vision
        for l, player in players do
            ScenarioFramework.CreateVisibleAreaAtUnit(5, unit, 90, GetArmyBrain(player))
        end
    end 

end
