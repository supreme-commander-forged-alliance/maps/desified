
local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')

local EffectUtilities = import('/lua/EffectUtilities.lua')
local EffectTemplates = import('/lua/EffectTemplates.lua')

do
    -- make a unit completely immune
    function MakeImmune(unit)
        unit:SetReclaimable(false)
        unit:SetCapturable(false)
        unit:SetDoNotTarget(true)
        unit:SetRegenRate(5000)
        unit:SetUnSelectable(true)
        unit:SetCanTakeDamage(false)
        unit:SetCanBeKilled(false)
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- find all players

	local armies = ListArmies()
	local players = { }
  
	for v, army in armies do
		if string.find(army, "ARMY") then
			table.insert(players, army)
		end
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- spawn the three structures

    local observer = ScenarioUtils.CreateArmyGroup("Neutrals", "Observer", false)[1]
    local protectors = ScenarioUtils.CreateArmyGroup("Neutrals", "Protector", false)
    local former = ScenarioUtils.CreateArmyGroup("Neutrals", "Former", false)[1]

    -- change the stats and sizes of the shields, essentially make them inpenetrable
    -- and add some cinematic feel to them
    protectors[1].MyShield.RegenRate = 2000
    protectors[1].MyShield.RegenStartTime = 0
    protectors[1].MyShield.ShieldVerticalOffset = 3.5
    protectors[1].MyShield.Size = 10

    protectors[2].MyShield.RegenRate = 2000
    protectors[2].MyShield.RegenStartTime = 0
    protectors[2].MyShield.ShieldVerticalOffset = -2
    protectors[2].MyShield.Size = 18

    -- the nano-bot effect
    local bone = observer:GetBoneName(1)
    EffectUtilities.CreateBoneEffects(observer, bone, "Neutrals", EffectTemplates.CSoothSayerAmbient)

    -- some power gen effect for laughs and giggles
    local boneCount = observer:GetBoneCount()
    for k = 1, boneCount, 1 do
        local name =  observer:GetBoneName(k - 1)
        EffectUtilities.CreateBoneEffects(observer, name, "Neutrals", EffectTemplates.CT2PowerAmbient)
    end

    -- the 'flare' that happens every four seconds
    function Loop()
        while true do 
            WaitSeconds(4.0)
            for k = 1, boneCount, 1 do
                local name =  observer:GetBoneName(k - 1)
                EffectUtilities.CreateBoneEffects(observer, name, "Neutrals", EffectTemplates.CloudFlareEffects01)
            end
        end
    end

    ForkThread(Loop)

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- make them immune, set their health values, etc

    MakeImmune(observer)
    MakeImmune(former)

    for k, protector in protectors do
        MakeImmune(protector)
        protector:SetMaxHealth(2000)
        protector:SetHealth(nil, 2000)
        protector:SetCustomName("Protector")
    end

    observer:SetMaxHealth(1400)
    observer:SetHealth(nil, 4200)
    observer:SetCustomName("Observer")

    former:SetMaxHealth(19950)
    former:SetHealth(nil, 19950)
    former:SetCustomName("Terraformer")
    former:SetProductionPerSecondEnergy(600)

    -- give vision (or not)
    -- local range = 10
    -- local duration = -1
    -- for l, player in players do
    --     ScenarioFramework.CreateVisibleAreaAtUnit(range, former, duration, GetArmyBrain(player))
    -- end

end
