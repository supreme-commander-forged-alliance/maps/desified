
--------------------------------------------------------------------------------
-- Made by (Jip) Willem Wijnia
-- See also: https://gitlab.com/w.b.wijnia/desified
-- 
-- Please do not remove this message
--------------------------------------------------------------------------------

local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- find all players

	local armies = ListArmies()
    local players = { }
    
    local players13 = { }
    local players24 = { }

	for v, army in armies do
        if string.find(army, "ARMY") then
            
            -- generic: it's a player
            table.insert(players, army)

            -- specifically: it's a player of this 'team'
            if string.find (army, "1") or string.find (army, "3") then
                table.insert(players13, army)
            end

            if string.find (army, "2") or string.find (army, "4") then
                table.insert(players24, army)
            end
		end
    end


    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- spawn the trucks

    local trucks13 = ScenarioUtils.CreateArmyGroup("NEUTRAL_CIVILIAN", "Trucks13", false)
    local positions13 = ScenarioUtils.ChainToPositions("TruckChain13")
    local endPositions13 = { 
        ScenarioUtils.MarkerToPosition("TruckChain13-E1"), 
        ScenarioUtils.MarkerToPosition("TruckChain13-E2")
    }

    local trucks24 = ScenarioUtils.CreateArmyGroup("NEUTRAL_CIVILIAN", "Trucks24", false)
    local positions24 = ScenarioUtils.ChainToPositions("TruckChain24")
    local endPositions24 = { 
        ScenarioUtils.MarkerToPosition("TruckChain24-E1"), 
        ScenarioUtils.MarkerToPosition("TruckChain24-E2")
    }

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- add behavior

    function Behavior(trucks, chain, ends, players)

        WaitSeconds(5.0)
        
        for k, truck in trucks do 

            -- move along the chain
            for l, position in chain do 
                IssueMove({truck}, position)
            end

            -- move to designated end location
            IssueMove({truck}, ends[k])

            -- wait a wee bit, cinematic effect!
            WaitSeconds(3.5)
        end
    end

    -- send them on their merry way
    ForkThread(Behavior, trucks13, positions13, endPositions13, players13)
    ForkThread(Behavior, trucks24, positions24, endPositions24, players24)

end
